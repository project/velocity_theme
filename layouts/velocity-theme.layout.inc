name = Velocity Theme Layout
description = A simple single column layout demonstrating mobile first and Susy.
preview = preview.png
template = velocity-theme-layout

; Regions
regions[logo] = Logo
regions[nav] = Nav
regions[content] = Content
regions[footer] = Footer
regions[copyright] = Copyright
