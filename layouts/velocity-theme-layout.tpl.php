<div<?php print $attributes; ?>>
  
  <header class="l-region-header" role="banner">
    <div class="contain">
      <div class="l-logo">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>
        <?php print render($page['logo']); ?>
      </div>
      <div class="l-nav">
        <?php print render($page['nav']); ?>
      </div>
    </div>
  </header>

  <div class="l-region-main">
    <div class="contain">
      <div class="l-content" role="main">
        <div class="l-messages">
          <?//php drupal_set_message("Example for status message", 'status'); ?>
          <?//php drupal_set_message("Example for warning message", 'warning'); ?>
          <?//php drupal_set_message("Example for error message", 'error'); ?>
          <?php print $messages; ?>
        </div>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div>
    </div>
  </div>

  <footer class="l-region-footer" role="contentinfo">
    <div class="contain">
      <div class="l-footer">
        <?php print render($page['footer']); ?>
      </div>
      <div class="l-copyright">
        <?php print render($page['copyright']); ?>
      </div>
    </div>
  </footer>

</div>
